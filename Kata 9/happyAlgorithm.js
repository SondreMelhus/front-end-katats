function happyAlgorithm (nr) { 
    console.log(recHappyAlgorithm(nr, 1, prevVal = new Set()));
}

function recHappyAlgorithm (nr, nrOfCalls, prevValues) {

    let square = [...nr.toString()].reduce((partialSum, n) => partialSum + parseInt(n**2), 0);

    if (square == 1) {
        return `HAPPY ${nrOfCalls}`;
    } else if (prevValues.has(square)) {
        return `SAD ${nrOfCalls}`
    } else {
        prevValues.add(square);
    }
   
    return recHappyAlgorithm(square, nrOfCalls + 1, prevValues);
}

happyAlgorithm(139); // -> 'HAPPY 5'
happyAlgorithm(67) //➞ "SAD 10"
happyAlgorithm(1); // -> 'HAPPY 1'
happyAlgorithm(89) //➞ "SAD 8"
