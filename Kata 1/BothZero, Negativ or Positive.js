// Write a function that returns true if both numbers are:
// •    Smaller than 0, OR ...
// •    Greater than 0, OR ...
// •    Exactly 0
// Otherwise, return false.
// Examples
// both(6, 2) ➞ true

// both(0, 0) ➞ true

// both(-1, 2) ➞ false

// both(0, 2) ➞ false
// Notes
// Inputs will always be two numbers.


// version 2
// Inputs will NOT always be two numbers.
// both("0", 2) --> false

function bothZeroNegativOrPositive (x, y) {
   
        typeof x == 'number' && 
        typeof y == 'number' &&
        (x > 0 && y > 0) || 
        (x == 0 && y == 0) || 
        (x < 0 && y < 0) ? console.log('True') : console.log('False');
} 

bothZeroNegativOrPositive("2", 2);
bothZeroNegativOrPositive (1,2);
bothZeroNegativOrPositive (0,2);
bothZeroNegativOrPositive (0,0);

let i = 15;
console.log(i %= 3);