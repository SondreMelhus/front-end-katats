//Create a function that accepts a string as an argument. 
// The function must move all capital letters to the front of a word, lowercase letters 
// thereafter and lastly, numbers to the back. It should return a string with the reordered 
// word.

// Examples

// reorder("hA2p4Py") ➞ "APhpy24"

// reorder ("m11oveMENT") ➞ "MENTmove11"

// reorder ("s9hOrt4CAKE") ➞ "OCAKEshrt94"

// Notes
// Keep the original relative order of the upper and lower case letters 
// the same as well as numbers

function reorder (pattern) {

    //Strings used to store substrings of a char type
    let upperCasePattern = '';
    let lowerCasePattern = '';
    let numberPattern = '';

    //RegEx patterns
    let upperCase = (/[A-Z]/);
    let lowerCase = (/[a-z]/);
    //Could add patterns for special characters or numbers if required

    //Loop through each char in the pattern and tests each char against regEx patterns
    for (let char of pattern) {

        //Upper case char test
        if (upperCase.test(char)){        
            upperCasePattern += char;
        } 
        //Lower case char test
        else if (lowerCase.test(char)) { 
            lowerCasePattern += char;
        }
        //If none of the above, assume its a number 
        else {                                    
            numberPattern += char;
        }
    }

    //Concat all pattern strings together in the correct order
    upperCasePattern += lowerCasePattern + numberPattern;
    console.log('Original pattern: ' + pattern + ', reordered pattern: ' + upperCasePattern);
}

reorder("hA2p4Py") //➞ "APhpy24"

reorder ("m11oveMENT") //➞ "MENTmove11"

reorder ("s9hOrt4CAKE") //➞ "OCAKEshrt94"