/*
Tic Tac Toe
Source: Edabit
Published by Harith in JavaScript
Arrays, games, logic, numbers

Given a 3x3 matrix of a completed tic-tac-toe game, create a function that returns whether 
the game is a win for "X", "O", or a "Draw", where "X" and "O" represent themselves on the 
matrix, and "E" represents an empty spot.
Examples
ticTacToe([
  ["X", "O", "X"],
  ["O", "X",  "O"],
  ["O", "X",  "X"]
]) ➞ "X"

ticTacToe([
  ["O", "O", "O"],
  ["O", "X", "X"],
  ["E", "X", "X"]
]) ➞ "O"

ticTacToe([
  ["X", "X", "X"],
  ["O", "O", ""],
  ["", "", ""]
]) ➞ "X"

ticTacToe([
  ["O", "O", "X"],
  ["X", "", "X"],
  ["O", "", "X"]
]) ➞ "X"


ticTacToe([
  ["X", "X", "O"],
  ["O", "O", "X"],
  ["X", "X", "O"]
]) ➞ "Draw"
Notes
•	Make sure that if O wins, you return the letter "O" and not the integer 0 (zero) and 
•	If it's a draw, make sure you return the capitalised word "Draw". 
•	If you return "X" or "O", make sure they're capitalised too.

*/

/*
Win states:
    - Three in a row horizontaly
    - Three in a row verticaly
    - Three in a row diagonaly

Possible win:
Arr 1: x [0]
Arr 2: x[0]
Arr 3: x[0]
*/

/*
Possible solution:
 - Split in three arrays
 - A loop that iterates through

 
*/ 

function ticTacToe (board) {

    for (let i = 0; i < 3; i++) {
        //Check horizontal
        if (resolveHorizontal(board[i], 'X')) { return 'X' };
        if (resolveHorizontal(board[i], 'O')) { return 'O'};
    }
    //Check vertical
    if (resolveVertical(board, 'X')) { return 'X'};
    if (resolveVertical(board, 'O')) { return 'O'};

    //Check diagonal
    if (resolveDiagonal(board, 'X')) {return 'X'};
    if (resolveDiagonal(board, 'O')) {return 'O'};

    return 'DRAW';
}

function resolveHorizontal (row, player) {
    if (row[0] == player && row[1] == player && row[2] == player) {
        return true;
    } else {
        return false;
    }
}

function resolveVertical (board, player) {
    if (board[0][0] == player && board[1][0] == player && board[2][0] == player ||
        board[0][1] == player && board[1][1] == player && board[2][1] == player ||
        board[0][2] == player && board[1][2] == player && board[2][2] == player) {
        return true;
    } else {
        return false;
    }    
}

function resolveDiagonal (board, player) {
    if (board[0][0] == player && board[1][1] == player && board[2][2] == player ||
        board[0][2] == player && board[1][1] == player && board[2][0] == player) {
        return true;
    } else {
        return false;
    }
}


//Test cases
console.log(ticTacToe([
    ["X", "O", "X"],
    ["O", "X",  "O"],
    ["O", "X",  "X"]
  ])); //➞ "X"
  
 console.log(ticTacToe([
    ["O", "O", "O"],
    ["O", "X", "X"],
    ["E", "X", "X"]
  ])); //➞ "O"
  
 console.log(ticTacToe([
    ["X", "X", "X"],
    ["O", "O", "E"],
    ["E", "E", "E"]
  ])); //➞ "X"
  
 console.log(ticTacToe([
    ["O", "O", "X"],
    ["X", "E", "X"],
    ["O", "E", "X"]
  ])); //➞ "X"

console.log(ticTacToe([
    ["X", "X", "O"],
    ["O", "O", "X"],
    ["X", "X", "O"]
  ])); //➞ "Draw"