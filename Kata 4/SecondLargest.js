/*
Create a function that takes an array of numbers and returns the second largest number.
Examples
secondLargest([10, 40, 30, 20, 50]) ➞ 40
 
secondLargest([25, 143, 89, 13, 105]) ➞ 105
 
secondLargest([54, 23, 11, 17, 10]) ➞ 23
 
secondLargest([1, 1]) ➞ 0
 
secondLargest([1]) ➞ 1
 
secondLargest([]) ➞ 0
 
Notes
If only one number exists, return that number

When array only has two numbers that are equal, return 0

Return 0 for an empty array
*/


function secondLargest(array) {

    let largest = 0;
    let secLargest = 0;
    let thrdLargest = 0;
    let first = false;

    for (let i of array) {
        if (array.length == 1) {
            secLargest = i;
        } else if (!first) {
            largest = i;
            first = true;
        } else if (i > largest) {
            thrdLargest = secLargest;
            secLargest = largest;
            largest = i;
        } else if (i > thrdLargest && i >secLargest && i < largest)  {
            thrdLargest = secLargest;
            secLargest = i;
        } else {

        }
    }

    console.log('The second largest value is ' + secLargest);;
}

secondLargest([10, 40, 30, 20, 50]) //➞ 40
 
secondLargest([25, 143, 89, 13, 105]) //➞ 105
 
secondLargest([54, 23, 11, 17, 10]) //➞ 23
 
secondLargest([1, 1]) //➞ 0
 
secondLargest([1]) //➞ 1
 
secondLargest([]) //➞ 0