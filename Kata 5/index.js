/*
Today's Kata: build a Calculator (as a Web App) using HTML, CSS and JS
2. Add a tape to the calculator
3. Add some extensions: PI, LN, LOG, SIN, COS, TAN, etc.
*/


'use strict'
const calc = new Calculator();

const calculatorButtons = document.getElementById('calculator');

calculatorButtons.addEventListener("click", (event) => {
  calc.handleInput(event);
});


function Calculator() {
    this.input = document.getElementById('inputField');

    this.handleInput = (event) => {
        let inputValue = event.target.value;
        let inputBox = document.getElementById('inputField').innerText;

        if (inputValue == '=') {
            this.calculate(inputBox);
        } else if (inputValue == 'C') {
            this.input = 0;
            document.getElementById('inputField').innerText = ('0');
        } else if (inputValue == 'DEL') {
            if (inputBox.length == 1) {
                inputBox = 0;
            } else {
                inputBox = inputBox.slice(0, -1);
            }
            document.getElementById('inputField').innerText = (inputBox);
        }
        else {
            if (inputValue != undefined) {
                if (inputBox == '0') {
                    inputBox = '';
                }
                inputBox += inputValue;
                document.getElementById('inputField').innerText = (inputBox);
        }
    };


    this.calculate = (expression) => {

        //RegEx patterns
        let digit = (/[0-9]/);
        let operator = (/[+/*^%!-]/);
        let comma = (/[.]/);
        let letter = (/[LogSinTanCos]/);

        let number1 = '';
        let number2 = '';
        let operand = '';

        let number1Completed = false;

        for (let part of expression) {
            if (operator.test(part) || letter.test(part)) {
                operand += part;
                number1Completed = true;
            } else if (digit.test(part) && number1Completed || comma.test(part) && number1Completed) {
                number2 += part;
            } else {
                number1 += part;
            }
        }

        let answer = 0.0;
        let parsedNr1 = parseFloat(number1);
        let parsedNr2 = parseFloat(number2);

        if (operand == '+') {answer = parsedNr1 + parsedNr2;}
        if (operand == '-') {answer = parsedNr1 - parsedNr2;}
        if (operand == '*') {answer = parsedNr1 * parsedNr2;}
        if (operand == '/') {answer = parsedNr1 / parsedNr2;}
        if (operand == '%') {answer = parsedNr1 % parsedNr2}
        if (operand == '!') {answer = this.factorial(parsedNr2)}
        if (operand == '^') {answer = Math.pow(parsedNr1, parsedNr2)}
        if (operand == 'Sin') {answer = Math.sin(parsedNr2)}
        if (operand == 'Cos') {answer = Math.cos(parsedNr2)}
        if (operand == 'Tan') {answer = Math.tan(parsedNr2)}
        if (operand == 'Log') {answer = Math.log(parsedNr2)}

        answer = Math.round((answer + Number.EPSILON) * 100) / 100
        document.getElementById('inputField').innerText = (answer);
    };

    this.factorial = (number) => {
        if (number == 0) {
            return -1;
        } else if (number == 1) {
            return 1;
        } else {
            return number * this.factorial(number - 1);
        }
    }
}
}
