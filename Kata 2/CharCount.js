// Interview, language_fundamentals, loops, strings

// Create a function that takes two strings as arguments and returns the number of times 
// the first string (the single character) is found in the second string.
// Examples
// charCount("a", "edabit") ➞ 1

// charCount("c", "Chamber of secrets") ➞ 1

// charCount("B", "boxes are fun") ➞ 0

// charCount("b", "big fat bubble") ➞ 4

// charCount("e", "javascript is good") ➞ 0

// charCount("!", "!easy!") ➞ 2
// Notes
// Your output must be case-sensitive (see second example).
// You can get the length of a string with the .length property

function charCount (char, pattern) {
    let occurenses = 0;

    for (let c in pattern) {
        if (pattern[c] === char) {
            occurenses++;
        }
    }

    console.log(char + ' occured ' + occurenses + ' times in the string: ' + pattern);
}

charCount("a", "edabit") // ➞ 1
charCount("c", "Chamber of secrets") // ➞ 1
charCount("B", "boxes are fun") // ➞ 0
charCount("b", "big fat bubble") // ➞ 4
charCount("e", "javascript is good") // ➞ 0
charCount("!", "!easy!") // ➞ 2