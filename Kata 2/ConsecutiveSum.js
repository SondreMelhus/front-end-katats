// bit_operations, logic, math, numbers, validation

// Create a function that takes a number n as an argument and checks whether the given 
// number can be expressed as a sum of two or more consecutive positive numbers.
// Examples
// consecutiveSum(9) ➞ true
// // 9 can be expressed as a sum of (2 + 3 + 4) or (4 + 5).

// consecutiveSum(10) ➞ true
// // 10 can be expressed as a sum of 1 + 2 + 3 + 4.

// consecutiveSum(64) ➞ false

/*
function consecutiveSum (n) {
    let res = fals;
    if (n < 3) return false;
    for (let k = 1; k < n; k++) {
        let sum = 0;
        for (let i = k; i < n; i++) {
            sum += i;
            if (sum == n) return true;
        }
    }
    return res;
}
*/

function consecutiveSum (number) {
    let possible = false;
    if (number < 3) return false;
    for (let i = 1; i < number; i++) {
        if (!possible) {
            possible = recPart(0, i, number);
        } else {
            break;
        }
    }
    console.log('Consecutive sum of ' + number + ' is ' + possible);
}

function recPart (sum, number, maxNumber) {
    if (sum + number > maxNumber) return false;
    if (sum + number == maxNumber) return true;
    return recPart(sum += number, number + 1, maxNumber);
}

consecutiveSum(9);
consecutiveSum(10);
consecutiveSum(64);
