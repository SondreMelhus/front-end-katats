### Project description

This project is used to store the solutions of the Front-end katas (tasks) given during my Full-stack
course at Noroff. The katas are given as a "warm up" before each daily lesson, and wary in difficulty. Each kata submission contains:

- Task descripton
- Task solution
- Example inputs to test the solution

### Status
I am still attending the Front-end part of the course, and will add more tasks as they are presented and solved.

### Code contribution
**Author:** Sondre Melhus

**Tasks supplied by:** Piotr Dziubinski, Lecturer at Noroff - School of technology and digital media
