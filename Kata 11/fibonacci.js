//Generate number x in the sequence
function fibonacci (number) {
    console.log('Binet: The ' + number + '. number in the fibonacci sequence is ' + binet (number));
    console.log('Iterative: The ' + number + '. number in the fibonacci sequence is ' + iterativeFibonacci (number));
    console.log('Recursive: The ' + number + '. number in the fibonacci sequence is ' + recFibonacci (number));
}


//Recursive solution
function recFibonacci (number) {
    if (number == 0) {
        return 0;
    }
    if (number == 1 || number == 2) {
        return 1;
    } else {
        return recFibonacci (number - 2) + recFibonacci (number - 1);
    }
}


//Iterativ solution
function iterativeFibonacci (number) {
    const sequence = [0, 1];

    for (i = 2; i <= number; i++) {
        sequence[i] = sequence[i - 2] + sequence[i - 1];
    }

    return sequence [number];
}


//For fun I found Binet's formula online and implemented it
//Binet's formula
function binet(number) {
    return Math.round((Math.pow((1 + Math.sqrt(5)) / 2, number) 
    - Math.pow((1 - Math.sqrt(5)) / 2, number)) / Math.sqrt(5));
}

fibonacci (10);  // -> 55
fibonacci (15);  // -> 610
fibonacci (20);  // -> 6765
fibonacci (50); // -> 12 586 269 025
