 /*
    Case 1: word.length <= 3 chars
    Case 2: word.length is odd
    Case 3: word.length is even
    */

function elasticate (word = '') {
    len = word.length;

    let left = '';
    let right = '';
    let center = '';

    let[subLeft, subCenter, subRight] = ['', '', ''];
    if (len%2 == 0) {
        subLeft = word.substring(0, len/2);
        subRight = word.substring(len/2);
    } else {
        subLeft = word.substring(0, len/2);
        subRight = word.substring(len/2+1);
        subCenter = word.substring(len/2, len/2+1);
        center = subCenter.repeat(Math.ceil(len/2));
    }

    for(let i = 0; i < subLeft.length; i++) {
        left += subLeft.charAt(i).repeat(i+1);
    }

    for(let i = 0; i < subRight.length; i++) {
        right += subRight.charAt(i).repeat(subRight.length - i);
    }

    console.log('Elasticated version of ' + word + ' is ' + left + center + right);
}

elasticate('ANNA');
elasticate('KAYAK');
elasticate('X');