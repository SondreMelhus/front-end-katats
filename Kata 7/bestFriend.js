function bestFriend (string, preChar, postChar) {

    return (((string.match(new RegExp(preChar+postChar, "g")) || []).length) 
    == ((string.match(new RegExp(preChar, "g")) || []).length)) ? true : false;
}

console.log(bestFriend("he headed to the store", "h", "e")); // -> True
console.log(bestFriend("i found an ounce with my hound", "o", "u")); // -> True
console.log(bestFriend("we found your dynamite", "d", "y")); // -> False